import { File } from "@ionic-native/file/ngx";
import {
  FileTransfer,
  FileTransferObject
} from "@ionic-native/file-transfer/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';

@Injectable({
  providedIn: "root"
})
export class PdfViewerService {
  fileTransfer: FileTransferObject;
  constructor(
    private fileOpener: FileOpener,
    private transfer: FileTransfer,
    private file: File, private http: HttpClient,
    private document: DocumentViewer
  ) { }


  downloadAndOpen(url: string, title: string) {
    this.fileTransfer = this.transfer.create();
    this.fileTransfer
      .download(url, this.file.dataDirectory + title + ".pdf")
      .then(entry => {
        console.log("download complete: " + entry.toURL());
 
        alert(entry.toURL());
        const options: DocumentViewerOptions = {
          title: 'My PDF'
        }

        this.document.viewDocument( entry.toURL(), 'application/pdf', options);
     
      }).catch(e => alert("Error opening file" + JSON.stringify(e)));
  }
}
